# AshamaneCoreLegacy

# Donate
https://www.paypal.me/zgn88
--------------


* [Build Status](#build-status)
* [Introduction](#introduction)
* [Requirements](#requirements)
* [Install](#install)
* [Reporting issues](#reporting-issues)
* [Submitting fixes](#submitting-fixes)
* [Copyright](#copyright)
* [Authors &amp; Contributors](#authors--contributors)
* [Links](#links)



## Build Status

[![Build Status](https://travis-ci.com/ReyDonovan/AshamaneCoreLegacy.svg?branch=master)](https://travis-ci.com/ReyDonovan/AshamaneCoreLegacy)
## Introduction

AshamaneCoreLegacy is a *MMORPG* Framework based mostly in C++.

It is fully based on *TrinityCore*

It is completely open source; community involvement is highly encouraged.

If you wish to contribute ideas or code please visit our site linked below or
make pull requests to our [Github repository](https://github.com/ReyDonovan/AshamaneCoreLegacy/pulls).

## Requirements

Software requirements are available in the [wiki](https://www.trinitycore.info/display/tc/Requirements) for
Windows, Linux and macOS.

## Install

Detailed installation guides are available in the [wiki](https://www.trinitycore.info/display/tc/Installation+Guide) for
Windows, Linux and macOS.

## Submitting fixes

C++ fixes are submitted as pull requests via Github.
For SQL only fixes open a ticket or if a bug report exists for the bug post on existing ticket.

## Copyright

License: GPL 2.0

Read file [COPYING](COPYING).

## Authors &amp; Contributors

Read file [THANKS](THANKS).

## Links

* [Wiki](https://www.trinitycore.info)
* [TrinityCore](https://www.trinitycore.org/)
* [Discord](https://discord.trinitycore.org/)
